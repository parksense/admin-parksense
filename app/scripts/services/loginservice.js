'use strict';

/**
 * @ngdoc service
 * @name parksenseApp.loginService
 * @description
 * # loginService
 * Service in the parksenseApp.
 */
angular.module('parksenseApp')
  .factory('loginService', function ($http,ApiEndpoint) {
    return {
      login : function(email, password) {
        return $http.post(ApiEndpoint.url + '/v1/users/authenticate', {
          email: email,
          password: password
        });
      }
    }  
  });
