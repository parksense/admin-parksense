'use strict';

/**
 * @ngdoc function
 * @name parksenseApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the parksenseApp
 */
angular.module('parksenseApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
