'use strict';

/**
 * @ngdoc function
 * @name parksenseApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the parksenseApp
 */
angular.module('parksenseApp')
  .controller('MainCtrl', function (access, $location) {
     var self = this;
     
     if (access != 200) {
       $location.path( '/login' );
     } 
  
  });
