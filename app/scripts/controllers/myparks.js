'use strict';

/**
 * @ngdoc function
 * @name parksenseApp.controller:MyparksCtrl
 * @description
 * # MyparksCtrl
 * Controller of the parksenseApp
 */
angular.module('parksenseApp')
  .controller('MyParksController', function ($http,ApiEndpoint,$cookies,$rootScope) {
    var self = this;
    self.loginError = false;
    
    getParks();
    self.listTitle = 'My Parks';
    self.list = [];
    self.park;
    self.parkId;
    
    var myDate = new Date();
    self.minDate = new Date(
      myDate.getFullYear(),
      myDate.getMonth(),
      myDate.getDate()
    );
    self.maxDate = new Date(
      myDate.getFullYear(),
      myDate.getMonth(),
      myDate.getDate()
    );
    
    self.updateMinDate = function() {
      $rootScope.$broadcast('updateMinDate', { minDate: self.minDate, parkId: self.parkId, park: self.park });
    }
    self.updateMaxDate = function() {
      $rootScope.$broadcast('updateMaxDate', { maxDate: self.maxDate, parkId: self.parkId, park: self.park });
    }
    
    self.percentage = function(datetime) {
      return moment(datetime).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }
    
    self.navigateToPark = function(park) {
      self.parkId = park._id;
      self.park = park;
      console.log(park)
      $rootScope.$broadcast('updateMaxDate', { maxDate: self.maxDate, parkId: self.parkId, park: self.park });
    }
    
    function getParks() {
      $http({
        method: 'POST',
        url: ApiEndpoint.url + '/v1/park/ownerparks',
        headers: {
          'Authorization': $cookies.get('token')
        }
      }).then(function successCallback(response) {
        if (!response.data.success) {
          console.log(response)
        } else {
          console.log(response)
          self.list = response.data.items;
          self.parkId = self.list[0]._id;
          self.park = self.list[0];
          console.log(self.park)
          $rootScope.$broadcast('updateMaxDate', { maxDate: self.maxDate, parkId: self.parkId, park: self.park });
        }
      }, function errorCallback(response) {
        console.log(response)
      });
    }
  });
