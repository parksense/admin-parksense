'use strict';

/**
 * @ngdoc function
 * @name parksenseApp.controller:LogincontrollerCtrl
 * @description
 * # LogincontrollerCtrl
 * Controller of the parksenseApp
 */
angular.module('parksenseApp')
  .controller('LogincontrollerCtrl', function (loginService,$location,$cookies) {
    var self = this;
    self.loginError = false;
    
    function handleRequest(res) {
      var token = res.data ? res.data.token : null;
      console.log("dffdhgfg")
      if(token) { 
        
        self.loginError = false;
        $cookies.put('token', token);
        $location.path( '/' );
      } else {
        self.loginError = true;
      }
    }
    
    function handleError(err) {
      console.log(err)
    }
    
    self.login = function() {
      loginService.login(self.email, self.password)
        .then(handleRequest, handleError)
    }
  });
