'use strict';

/**
 * @ngdoc directive
 * @name parksenseApp.directive:map
 * @description
 * # map
 */
angular.module('parksenseApp')
  .directive('map', function ($http,$cookies,ApiEndpoint) {
    return {
      template: '<div id="mapid"></div>',
      restrict: 'E',
      link: function (scope, element, attrs) {
        var map = L.map('mapid').setView([51.505, -0.09], 13);
        var plotlist;
        var plotlayers=[];

        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a>',
          maxZoom: 18
        }).addTo(map);
        
        map.setView(new L.LatLng(38.521593, -8.839177),12);
        
        askForPlots();
	      map.on('moveend', onMapMove);
        
        function onMapMove(e) { askForPlots(); }
        
        function removeMarkers() {
          for (var i = 0; i < plotlayers.length; i++) {
            map.removeLayer(plotlayers[i]);
          }
          plotlayers=[];
        }
        
        scope.$on('navigateToPark', function(event, args) {
          console.log(args)
            map.setView(new L.LatLng(args.park.loc[1], args.park.loc[0]));
        });

        function stateChanged(plotlist) {
          removeMarkers();
          
          for (var i = 0; i < plotlist.length; i++) {
            var plotll = new L.LatLng(plotlist[i].loc[1],plotlist[i].loc[0], true);
            
            var greenIcon = L.icon({
                iconUrl: 'http://www.myiconfinder.com/uploads/iconsets/a5485b563efc4511e0cd8bd04ad0fe9e.png',

                iconSize:     [42, 42], // size of the icon
                iconAnchor:   [22, 38], // point of the icon which will correspond to marker's location
                popupAnchor:  [-3, -36] // point from which the popup should open relative to the iconAnchor
            });

            var plotmark = new L.Marker(plotll, {icon: greenIcon});
            plotmark.data=plotlist[i];
            map.addLayer(plotmark);
            plotmark.bindPopup("<h3>"+plotlist[i].name+"</h3> Occupation: "+plotlist[i].occupation+'/'+plotlist[i].size);
            plotlayers.push(plotmark);
            
          }
        }
        
        function askForPlots() {
          var bounds=map.getBounds();
          var minll=bounds.getSouthWest();
          var maxll=bounds.getNorthEast();
          $http({
            method: 'POST',
            url: ApiEndpoint.url + '/v1/park/ownerparksbox',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              bllon: minll.lng,
              bllat: minll.lat,
              urlon: maxll.lng,
              urlat: maxll.lat
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              console.log(response)
            } else {
              stateChanged(response.data.items)
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
        
      }
    };
  });
