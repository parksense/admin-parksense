'use strict';

/**
 * @ngdoc directive
 * @name parksenseApp.directive:navToolbar
 * @description
 * # navToolbar
 */
angular.module('parksenseApp')
  .directive('navToolbar', function ($cookies) {
    return {
      template: '<div><md-toolbar>\
  <div class="md-toolbar-tools">\
    <h2>Parksense Admin Panel</h2>\
    <span flex></span>\
    <a href="/#/">\
      <md-button ng-show="loggedin" class="md-primary md-hue-2" aria-label="My Parks" style="font-weight: 600;">\
        Dashboard\
      </md-button> \
    </a>\
    <a href="/#/parks">\
      <md-button ng-show="loggedin" class="md-primary md-hue-2" aria-label="My Parks" style="font-weight: 600;">\
        My Parks\
      </md-button> \
    </a>\
    <md-menu md-position-mode="target-right target" >\
      <md-button ng-show="loggedin" class="md-icon-button" aria-label="More">\
        <ng-md-icon icon="more_vert" style="fill: rgba(255,255,255,0.85)" size="24" ng-click="$mdOpenMenu($event)"></ng-md-icon> \
      </md-button>\
      <md-menu-content width="3" style="background-color: #fff !important">\
        <md-menu-item>\
          <md-button >\
            <div layout="row" flex ng-click="logout()">\
              <p flex>Logout</p>\
              <ng-md-icon md-menu-align-target icon="logout" style="fill: rgba(0,0,0,0.8)" size="20"></ng-md-icon>\
            </div>\
          </md-button>\
        </md-menu-item>\
      </md-menu-content>\
    </md-menu>\
  </div>\
</md-toolbar></div>',
      restrict: 'E',
      link: function (scope, element, attrs) {
        scope.loggedin = false;
        if ($cookies.get('token')) {
          scope.loggedin = true;
        }
        
        scope.logout = function() {
          $cookies.remove('token');
          location.assign("/");
        }
      }
    };
  });
