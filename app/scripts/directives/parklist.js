'use strict';

/**
 * @ngdoc directive
 * @name parksenseApp.directive:parkList
 * @description
 * # parkList
 */
angular.module('parksenseApp')
  .directive('parkList', function ($http,ApiEndpoint,$cookies,$rootScope) {
    return {
      template: '<div><md-list class="md-dense" flex style="padding-top:0px">\
  <md-subheader class="md-no-sticky">{{listTitle}}</md-subheader>\
  <md-divider ></md-divider>\
  <md-list-item ng-click="navigateToPark(item)" class="md-2-line" ng-repeat="item in list">\
    <div class="md-list-item-text" >\
      <h3> {{item.name}} - <small>Occupation: {{item.occupation}}/{{item.size}}</small></h3>\
      <p> \
        Created At: {{percentage(item.createdAt)}} \
      </p>\
    </div>\
</md-list></div>',
      restrict: 'E',
      link: function (scope, element, attrs) {
        getParks();
        
        scope.listTitle = 'My Parks';
        scope.list = [];
        
        scope.percentage = function(datetime) {
          return moment(datetime).format("dddd, MMMM Do YYYY, h:mm:ss a");
        }
        
        function getParks() {
          $http({
            method: 'POST',
            url: ApiEndpoint.url + '/v1/park/ownerparks',
            headers: {
              'Authorization': $cookies.get('token')
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              console.log(response)
            } else {
              console.log(response)
              scope.list = response.data.items;
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
        
        scope.navigateToPark = function(park) {
          $rootScope.$broadcast('navigateToPark', { park: park });
        }
      }
    };
  });
