'use strict';

/**
 * @ngdoc directive
 * @name parksenseApp.directive:mapTimetable
 * @description
 * # mapTimetable
 */
angular.module('parksenseApp')
  .directive('mapTimetable', function ($http,ApiEndpoint,$cookies,$rootScope) {
    return {
      template: '<div><div layout="row">\
  <md-list style="max-width: 100px;" ng-cloak >\
    <md-list-item >\
      Schedule\
    </md-list-item>\
    <md-list-item ng-repeat="(key, value) in defaultTimetable" style="min-height: 30px !important;">\
      {{key}}\
    </md-list-item>\
  </md-list>\
  <md-list ng-cloak style="max-width: 120px;" ng-repeat="item in timeTable">\
    <div >\
      <md-list-item>\
        {{item.date}}\
      </md-list-item>\
      <md-list-item ng-repeat="(key, value) in item.time" style="min-height: 30px !important;">\
        {{value}}% \
      </md-list-item>\
    </div>\
  </md-list>\
</div>\
</div>',
      restrict: 'E',
      link: function (scope, element, attrs) {
        console.log(attrs)
        scope.minDate = moment(attrs.minDate);
        scope.maxDate = moment(attrs.minDate);
        scope.park = attrs.park;
        scope.parkId = scope.park._id;
        scope.logs;
        scope.highestOccupation = 0;
        scope.timeTable;
        scope.defaultTimetable;
        scope.occupation;
        console.log(scope.park)
        updateLogs();
        
        scope.$on('updateMinDate', function(event, args) {
          console.log(args)
          scope.minDate = moment(args.minDate);
          scope.park = args.park;
          scope.parkId = args.parkId;
          updateLogs();
        });
        
        scope.$on('updateMaxDate', function(event, args) {
          console.log(args)
          scope.maxDate = moment(args.maxDate);
          scope.park = args.park;
          scope.parkId = args.parkId;
          updateLogs();
        });
        
        function updateLogs() {
          $http({
            method: 'POST',
            url: ApiEndpoint.url + '/v1/park/logs/range',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              id: scope.parkId,
              minDate: scope.minDate,
              maxDate: scope.maxDate
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
            } else {
              scope.logs = response.data.items;
              generateTimetable();
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
      
        function generateTimetable() {
          
          var defaultTimetable = { 
            "00:00":0, 
            "01:00":0, 
            "02:00":0, 
            "03:00":0, 
            "04:00":0, 
            "05:00":0, 
            "06:00":0, 
            "07:00":0, 
            "08:00":0, 
            "09:00":0, 
            "10:00":0, 
            "11:00":0, 
            "12:00":0, 
            "13:00":0, 
            "14:00":0, 
            "15:00":0,
            "16:00":0,
            "17:00":0,
            "18:00":0,
            "19:00":0,
            "20:00":0,
            "21:00":0,
            "22:00":0,
            "23:00":0
          };
          
          /*
          var defaultTimetable = [
            { date: "", time: "00:00", occupation: 0},
            { date: "", time: "01:00", occupation: 0},
            { date: "", time: "02:00", occupation: 0},
            { date: "", time: "03:00", occupation: 0},
            { date: "", time: "04:00", occupation: 0},
            { date: "", time: "05:00", occupation: 0},
            { date: "", time: "06:00", occupation: 0},
            { date: "", time: "07:00", occupation: 0},
            { date: "", time: "08:00", occupation: 0},
            { date: "", time: "09:00", occupation: 0},
            { date: "", time: "10:00", occupation: 0},
            { date: "", time: "11:00", occupation: 0},
            { date: "", time: "12:00", occupation: 0},
            { date: "", time: "13:00", occupation: 0},
            { date: "", time: "14:00", occupation: 0},
            { date: "", time: "15:00", occupation: 0},
            { date: "", time: "16:00", occupation: 0},
            { date: "", time: "17:00", occupation: 0},
            { date: "", time: "18:00", occupation: 0},
            { date: "", time: "19:00", occupation: 0},
            { date: "", time: "20:00", occupation: 0},
            { date: "", time: "21:00", occupation: 0},
            { date: "", time: "22:00", occupation: 0},
            { date: "", time: "23:00", occupation: 0}
          ];
          */
          var timeTable = [];
          var differentDays = [];
          
          // Run all logs
          for (var i = 0; i < scope.logs.length; i++) {
            differentDays.push(moment(scope.logs[i].date).format('DD-MM-YYYY'));
          }
          
          // Remove duplicate dates
          differentDays = differentDays.filter (function (value, index, array) { 
            return differentDays.indexOf (value) == index;
          });

          for (var x = 0; x < differentDays.length; x++) {
            timeTable.push({ "date": differentDays[x], "time": defaultTimetable });
          }
/*
          // Add the unique dates to the timeTable arrays
          for (var x = 0; x < differentDays.length; x++) {
            console.log(differentDays[x])
            console.log({ "date": differentDays[x], "time": setDate(defaultTimetable, differentDays[x-1]) })
            timeTable.push({ "date": differentDays[x], "time": setDate(defaultTimetable, differentDays[x]) });
            console.log(timeTable)
          }
          
          function setDate(array, date) {
            for (var p = 0; p < array.length; p++) {
              array[p].date = date;
            }
            return array;
          }
          
          // Remove repetitions and end up with the sum of all datetime occupation logs in a single object array
          var occupation = [];
          for (var k = 0; k < scope.logs.length; k++) {
            occupation.push({ date: moment(scope.logs[k].date).format('DD-MM-YYYY'), time: moment(scope.logs[k].date).format('HH') + ":00", occupation: scope.logs[k].count });
          }
          
          var repeatsArray = [];
          var repeats = false;
          do {
            for (var l = 0; l < occupation.length; l++) {
              if (typeof occupation[l+1] != 'undefined') {
                if (occupation[l].date == occupation[l+1].date && occupation[l].time == occupation[l+1].time) {
                  occupation[l+1].index = l+1;
                  repeatsArray.push(occupation[l+1]);
                  repeats = true;
                }
              }
            }
            repeats = false;
          } while(repeats);
          
          for (var m = repeatsArray.length - 1; m >= 0; m--) {
            occupation.splice(repeatsArray[m].index, 1);
          }
          
          for (var n = 0; n < occupation.length; n++) {
            for (var o = 0; o < repeatsArray.length; o++) {
              if (occupation[n].date == repeatsArray[o].date && occupation[n].time == repeatsArray[o].time) {
                occupation[n].occupation += repeatsArray[o].occupation;
              }
            }
          }
          
          scope.occupation = occupation;
          console.log(timeTable)
          console.log(occupation)
          */
          
          // Add the occupations to the corresponding date and time
          for (var l = 0; l < timeTable.length; l++) {
            for (var k = 0; k < scope.logs.length; k++) {
              if (moment(scope.logs[k].date).format('DD-MM-YYYY') == timeTable[l].date) {
                timeTable[l].time[moment(scope.logs[k].date).format('HH') + ":00"] += scope.logs[k].count;
              }
            }
          }
          
          // Transform occupations in percentages
          if (scope.park != null) {
            for (var i = 0; i < timeTable.length; i++) {
              for (var key in timeTable[i].time) {
                timeTable[i].time[key] = Math.round(((timeTable[i].time[key] * 100 ) / scope.park.size )* 10 ) / 10;
              }
            }
          }
          
          // Store the highest occupation for the color intensity
          for (var i = 0; i < timeTable.length; i++) {
            for (var key in timeTable[i].time) {
              if (timeTable[i].time[key] > scope.highestOccupation) {
                scope.highestOccupation = timeTable[i].time[key];
              }
            }
          }
          
          
          scope.timeTable = timeTable;
          scope.defaultTimetable = defaultTimetable;
          //console.log(timeTable);
          //console.log(scope.highestOccupation)
          
        }
      
      }
    };
  });
