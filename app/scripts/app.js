'use strict';

/**
 * @ngdoc overview
 * @name parksenseApp
 * @description
 * # parksenseApp
 *
 * Main module of the application.
 */
angular
  .module('parksenseApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMdIcons',
    'ngMaterial',
    'angular-jwt',
    'ngStorage',
    'ngCookies'
  ])
  .constant('ApiEndpoint', {
    url: 'http://api-parksense.herokuapp.com/api'
    //url: 'http://localhost:3000/api'
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        resolve: {
          access: ['Access', function (Access) { return Access.isAuthenticated(); }],
        }
      })
      .when('/parks', {
        templateUrl: 'views/myparks.html',
        controller: 'MyParksController',
        controllerAs: 'main',
        resolve: {
          access: ['Access', function (Access) { return Access.isAuthenticated(); }],
        }
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LogincontrollerCtrl',
        controllerAs: 'main',
        resolve: {
          access: ['Access', function (Access) { return Access.isAnonymous(); }],
        }
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .factory('Access', ['$cookies', function ($cookies) {
    var Access = {

      OK: 200,
      UNAUTHORIZED: 401,
      FORBIDDEN: 403,

      isAuthenticated: function () {
        if ($cookies.get('token')) {
          return Access.OK;
        } else {
          return Access.UNAUTHORIZED;
        }
      },
      
      isAnonymous: function () {
        if (!$cookies.get('token')) {
          return Access.OK;
        } else {
          return Access.UNAUTHORIZED;
        }
      }

    };

    return Access;

  }])
  .run(['$rootScope', 'Access', '$location', function ($rootScope, Access, $location) {

    $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
      if (rejection === Access.UNAUTHORIZED) {
        $location.path('/login');
      } 
    });

  }]);
