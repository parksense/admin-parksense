'use strict';

describe('Controller: MyparksCtrl', function () {

  // load the controller's module
  beforeEach(module('parksenseApp'));

  var MyparksCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MyparksCtrl = $controller('MyparksCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MyparksCtrl.awesomeThings.length).toBe(3);
  });
});
