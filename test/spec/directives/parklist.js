'use strict';

describe('Directive: parkList', function () {

  // load the directive's module
  beforeEach(module('parksenseApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<park-list></park-list>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the parkList directive');
  }));
});
