'use strict';

describe('Directive: mapTimetable', function () {

  // load the directive's module
  beforeEach(module('parksenseApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<map-timetable></map-timetable>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the mapTimetable directive');
  }));
});
